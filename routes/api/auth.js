const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const { check, validationResult } = require("express-validator");

// get auth user
router.get("/", auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select("-password");
    res.json({ status: 200, user });
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
});

// login
router.post(
  "/login",
  [
    check("username", "Username is required").exists(),
    check("password", "Password is required").exists(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { username, password } = req.body;

    try {
      // See if user exist
      let user = await User.findOne({ username });
      if (!user) {
        res.status(400).json({
          status: 400,
          errors: [{ msg: "User not exist" }],
        });
      }

      const isMatch = await bcrypt.compare(password, user.password);

      if (!isMatch) {
        res.status(400).json({
          status: 400,
          errors: [{ msg: "Invalid password" }],
        });
      }

      // Return JWT
      const payload = {
        user: {
          id: user.id,
        },
      };

      let _user = await User.findOne({ username }).select("-password");

      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: 360000 },
        (error, token) => {
          if (error) {
            throw error;
          } else {
            res.json({ status: 200, _user, token });
          }
        }
      );
    } catch (error) {
      console.log(error.message);
      res.status(500).json({
        status: 500,
        msg: "Server error",
      });
    }
  }
);

// register
router.post(
  "/register",
  [
    check("name", "Name is required").not().isEmpty(),
    check("username", "Username is required").not().isEmpty(),
    check("email", "Please enter a valid email").isEmail(),
    check(
      "password",
      "Please enter password with 6 or more characters"
    ).isLength({ min: 6 }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, username, password } = req.body;

    try {
      // See if user exist
      let user = await User.findOne({ username });

      if (user) {
        res.status(400).json({
          errors: [{ msg: "User already exist" }],
        });
      }

      user = new User({
        name,
        email,
        username,
        password,
      });

      // Encrypt password
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      await user.save();

      // Return JWT
      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: 360000 },
        (error, token) => {
          if (error) {
            throw error;
          } else {
            res.json({ token });
          }
        }
      );
    } catch (error) {
      console.log();
      res.status(500).json({
        msg: error.message,
      });
    }
  }
);

module.exports = router;
