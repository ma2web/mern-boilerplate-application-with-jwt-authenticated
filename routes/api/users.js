const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");

const User = require("../../models/User");

// get all
router.get("/", auth, async (req, res) => {
  let users = await User.find({}, (error, data) => {
    if (error) {
      res.status(400).json({ status: 400, error });
    } else {
      res.status(200).json({
        status: 200,
        data,
      });
    }
  });
});

// get one
router.get("/:id", auth, async (req, res) => {
  let user = await User.findOne({ _id: req.params.id }, (error, data) => {
    if (error) {
      res.status(400).json({ status: 400, error });
    } else {
      res.status(200).json({
        status: 200,
        data,
      });
    }
  });
});

module.exports = router;
