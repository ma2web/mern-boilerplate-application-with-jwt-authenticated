const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const { check, validationResult } = require("express-validator");

const Post = require("../../models/Post");

// create post
router.post(
  "/",
  auth,
  [check("title", "Title is required").exists()],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    await Post.create(req.body, (err, data) => {
      if (err) {
        res.status(422).json({ status: 422, msg: err.message });
      } else {
        res.json({ status: 200, data });
      }
    });
  }
);

// get all posts
router.get("/", auth, async (req, res) => {
  await Post.find((err, data) => {
    if (err) {
      res.status(422).json({ status: 422, msg: err.message });
    } else {
      res.json({ status: 200, data });
    }
  });
});

// get one post
router.get("/:id", auth, async (req, res) => {
  await Post.findOne({ _id: req.params.id }, (err, data) => {
    if (err) {
      res.status(404).json({ status: 404, msg: err.message });
    } else {
      res.json({ status: 200, data });
    }
  });
});

// update post
router.put("/:id", auth, async (req, res) => {
  await Post.findOneAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
    (err, data) => {
      if (err) {
        res.status(404).json({ status: 404, msg: err.message });
      } else {
        res.json({ status: 200, data });
      }
    }
  );
});

// delete post
router.delete("/:id", auth, async (req, res, next) => {
  await Post.findByIdAndRemove(req.params.id, (err, data) => {
    if (err) {
      res.status(404).json({ msg: err.message });
    } else {
      res.json({
        status: 200,
        data,
      });
    }
  });
});

// get post by user
router.get("/user/:id", auth, async (req, res) => {
  await Post.find({ user_id: req.params.id }, (err, data) => {
    if (err) {
      res.status(404).json({ status: 404, msg: err.message });
    } else {
      res.json({ status: 200, data });
    }
  });
});

module.exports = router;
