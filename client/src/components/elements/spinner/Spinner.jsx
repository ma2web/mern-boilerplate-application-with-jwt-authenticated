import React from "react";
import PropTypes from "prop-types";
import spinner from "../../../assets/img/spinner.svg";

const Spinner = (props) => {
  return (
    <div className='loader-container'>
      <img src={spinner} alt='spinner' />
    </div>
  );
};

Spinner.propTypes = {};

export default Spinner;
