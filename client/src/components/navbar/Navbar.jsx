import React from "react";
import PropTypes from "prop-types";
import { Link, NavLink } from "react-router-dom";
import { logout } from "../../redux/actions/auth";
import { connect } from "react-redux";
import logo from "../../assets/img/logo.png";

const Navbar = ({ logout }) => {
  return (
    <nav className='navbar navbar-expand-lg navbar-light'>
      <a className='navbar-brand' href='#'>
        <img
          src={logo}
          alt='ma2web'
          className='d-inline-block align-top'
          width='50'
        />
      </a>
      <button
        class='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#navbarText'
        aria-controls='navbarText'
        aria-expanded='false'
        aria-label='Toggle navigation'>
        <span class='navbar-toggler-icon'></span>
      </button>
      <div className='collapse navbar-collapse' id='navbarText'>
        <ul className='navbar-nav mr-auto'>
          <li className='nav-item'>
            <Link className='nav-link' to='/users'>
              users
            </Link>
          </li>
          <li className='nav-item'>
            <Link className='nav-link' to='/posts'>
              posts
            </Link>
          </li>
        </ul>
        <span className='navbar-text'>
          <a onClick={logout} href='/login'>
            <i className='fas fa-sign-out-alt'></i>{" "}
            <span className='hide-sm'>Logout</span>
          </a>
        </span>
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  logout: PropTypes.func.isRequired,
};

export default connect(null, { logout })(Navbar);
