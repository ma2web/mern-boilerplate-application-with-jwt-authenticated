import React from "react";
import PropTypes from "prop-types";
import Navbar from "../navbar/Navbar";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Users from "../pages/users/Users";
import Posts from "../pages/posts/Posts";
import { connect } from "react-redux";
import PrivateRoute from "../../PrivateRoute";

const Dashboard = ({ auth }) => {
  return (
    <Router>
      <div className='main-navbar'>
        <Navbar />
      </div>

      <div className='contents'>
        <Switch>
          <PrivateRoute path='/users' component={Users} exact />
          <PrivateRoute path='/posts' component={Posts} exact />
        </Switch>
      </div>
    </Router>
  );
};

Dashboard.propTypes = {};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {})(Dashboard);
