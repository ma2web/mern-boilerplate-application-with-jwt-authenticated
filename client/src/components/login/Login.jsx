import React from "react";
import PropTypes from "prop-types";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

const Login = (props) => {
  return (
    <div className='login'>
      <div className='row justify-content-center align-items-center'>
        <div className='col col-sm-12 col-md-3'>
          <nav>
            <div className='nav nav-tabs' id='nav-tab' role='tablist'>
              <a
                className='nav-item nav-link active'
                id='nav-login-tab'
                data-toggle='tab'
                href='#nav-login'
                role='tab'
                aria-controls='nav-login'
                aria-selected='true'>
                Login
              </a>
              <a
                className='nav-item nav-link'
                id='nav-register-tab'
                data-toggle='tab'
                href='#nav-register'
                role='tab'
                aria-controls='nav-register'
                aria-selected='false'>
                Register
              </a>
            </div>
          </nav>
          <div className='tab-content' id='nav-tabContent'>
            <div
              className='tab-pane fade show active pt-4'
              id='nav-login'
              role='tabpanel'
              aria-labelledby='nav-login-tab'>
              <LoginForm />
            </div>
            <div
              className='tab-pane fade pt-4'
              id='nav-register'
              role='tabpanel'
              aria-labelledby='nav-register-tab'>
              <RegisterForm />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Login.propTypes = {};

export default Login;
