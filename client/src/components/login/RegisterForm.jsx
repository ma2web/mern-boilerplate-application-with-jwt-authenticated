import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { register } from "../../redux/actions/auth";
import { Redirect } from "react-router-dom";

const RegisterForm = ({ register, isAuthenticated }) => {
  const [register_info, set_register_info] = useState({
    name: "",
    email: "",
    username: "",
    password: "",
  });

  const onChange = (e) => {
    set_register_info({
      ...register_info,
      [e.target.id]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    console.log(register_info);

    register(register_info);
  };

  console.log(isAuthenticated);

  if (isAuthenticated) {
    return <Redirect to='/' />;
  }

  return (
    <form onSubmit={onSubmit}>
      <div className='form-group'>
        <div className='mb-2'>
          <label className='sr-only' htmlFor='name'>
            Name
          </label>
          <div className='input-group'>
            <div className='input-group-prepend'>
              <div className='input-group-text'>@</div>
            </div>
            <input
              type='text'
              className='form-control'
              id='name'
              value={register_info.name}
              placeholder='name'
              onChange={onChange}
            />
          </div>
        </div>
        <div className='mb-2'>
          <label className='sr-only' htmlFor='email'>
            Email
          </label>
          <div className='input-group'>
            <div className='input-group-prepend'>
              <div className='input-group-text'>@</div>
            </div>
            <input
              type='email'
              className='form-control'
              id='email'
              value={register_info.email}
              placeholder='email'
              onChange={onChange}
            />
          </div>
        </div>
        <div className='mb-2'>
          <label className='sr-only' htmlFor='username'>
            Username
          </label>
          <div className='input-group'>
            <div className='input-group-prepend'>
              <div className='input-group-text'>@</div>
            </div>
            <input
              type='text'
              className='form-control'
              id='username'
              value={register_info.username}
              placeholder='username'
              onChange={onChange}
            />
          </div>
        </div>
        <div className='mb-2'>
          <label className='sr-only' htmlFor='password'>
            Password
          </label>
          <div className='input-group'>
            <div className='input-group-prepend'>
              <div className='input-group-text'>@</div>
            </div>
            <input
              type='password'
              className='form-control'
              id='password'
              value={register_info.password}
              placeholder='password'
              onChange={onChange}
            />
          </div>
        </div>
        <div className='mb-2'>
          <button type='submit' className='btn btn-dark btn-block'>
            Register
          </button>
        </div>
      </div>
    </form>
  );
};

RegisterForm.propTypes = {
  register: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { register })(RegisterForm);
