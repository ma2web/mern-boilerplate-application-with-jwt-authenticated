const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let Post = new Schema(
  {
    user_id: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
    title: {
      type: String,
      required: true,
    },
    body: {
      type: String,
    },
    category: {
      type: Array,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Post", Post);
