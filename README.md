# MERN boilerplate application with JWT authenticated

MERN boilerplate application with JWT authenticated - MongoDB, Express, ReacrJS, NodeJS


## Backend Dependencies
"bcryptjs", "concurrently", "config", "express", "express-validator", "jsonwebtoken", "mongoose", "nodemon"
## Frontend Dependencies
"axios" , "bootstrap", "jquery", "node-sass", "popper.js", "react-notifications", "react-redux", "react-router-dom", "redux", "redux-thunk", 

Run Both Frontend and Backend servers with ``npm run dev`` command.

Frontend port: http://localhost:3000
Backend port: http://localhost:5000